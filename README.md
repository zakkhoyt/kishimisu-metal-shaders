# About

The shader code in this project was adapted from kishimisu's youtube video guide, [An introduction to Shader Art Coding](https://www.youtube.com/watch?v=f4s1h2YETNY). The code in kishimisu's video is based on WebGL Shader Language, but in this repo that code has been converted to Metal Shader Language. 

See kishimisu's final shader on [shadertoy.com](https://www.shadertoy.com/view/mtyGWy).


# Preview
Here is a preview of the final shader running as a Swift app on MacOS. 

# Requirements
Metal Shaders were introduced in Xcode 15 / Swift 5.9 / MacOS 14 / iOS 17


<!-- <video width="320" height="240" controls>
  <source src="https://www.youtube.com/watch?v=lF4k4nKmxNE" type="video/mp4">
</video> -->

Click on the image below to view on [youtube](https://www.youtube.com/watch?v=lF4k4nKmxNE)

[![Youtube Demo](Documents/Images/SwiftUI_demo.png)](https://www.youtube.com/watch?v=lF4k4nKmxNE "Blinking LEDs")




# WebGL Shaders vs Metal Shaders
As I worked through converting this shader from GLSL to MetalSL, I [took note of the problems I encountered and how I solved them](./GLSL_TO_METAL.md).