//
//  kishimisu.metal
//  MetalShaders
//
//  Created by Zakk Hoyt on 7/16/23.
//
//  This demo was ported over from a GLSL video: https://www.youtube.com/@kishimisu
//  See README.md for details

#include <metal_stdlib>
#include <SwiftUI/SwiftUI_Metal.h>

using namespace metal;

// About: https://iquilezles.org/articles/palettes/
// Web app: http://dev.thi.ng/gradients/
// A modification of the above instead of passing them in.
float3 palette(float t) {
    float3 a = float3(0.5, 0.5, 0.5);
    float3 b = float3(0.5, 0.5, 0.5);
    float3 c = float3(1.0, 1.0, 1.0);
    float3 d = float3(0.263, 0.416, 0.557);
    return a + b * cos( 6.28318*(c * t + d));
}

// This function contains a set of commented out blocks of code.
// Each of these correlates to a step in the video.
// The last (uncommented) block makes use of some uniform values
// passed into this function.
[[ stitchable ]]
half4 kishimisu(
    float2 position,
    half4 currentColor,
    float4 iBoundingRect,
    float iTime,
    float loops,
    float sinFreq,
    float colorTime
) {
//    // Calc uv (0...1). Shader toy passed in iResolution. Us too.
//    float2 uv = position / iResolution.xy;
//    // Flip y axis since Metal puts 0 at the top of the view.
//    uv.y = 1.0 - uv.y;
//
//    // Convert uv from 0...1 to -1...1
//    uv = (uv - 0.5) * 2.0;
//    return half4(uv.x, uv.y, 0, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    float2 uv = (position / iResolution.xy * 2.0 - 1.0) * float2(1, -1);
//    return half4(half2(uv), 0, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    float2 uv = (position / iResolution.xy * 2.0 - 1.0) * float2(1, -1);
//    // length calculates the vector distance.
//    float d = length(uv);
//    return half4(d, 0, 0, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    float2 uv = (position / iResolution.xy * 2.0 - 1.0) * float2(1, -1);
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    uv.x *= iResolution.x / iResolution.y;
//    // length calculates the vector distance.
//    float d = length(uv);
//    return half4(d, 0, 0, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // length calculates the vector distance.
//    float d = length(uv);
//    return half4(d, 0, 0, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // length calculates the vector distance.
//    // sine-distance functions: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbE1rZVNSVjctNkJaUGFaYUlid1BCRDJRbUJ6QXxBQ3Jtc0tucnAyY1FhT1FnYWlEM2g4bnE2cnkxbVZ5NDctUjF5ZzZnOTA1V2NrNEpBQ0l6SUQ1RndtYkdxWVFWaGVMSWhaNmhIZzZ1bFJFY18yOHBnb19VUGl4RW1yTWJxaDNVcDZ3MXdUZDhMM0lZN3NyVFVnUQ&q=https%3A%2F%2Fiquilezles.org%2Farticles%2Fdistfunctions2d%2F&v=f4s1h2YETNY
//    float d = length(uv);
//    d -= 0.5;
//    // Adding abs changes from circle to a ring.
//    d = abs(d);
//    // step will remove any gradient making a very sharp ring.
//    // d = step(0.1, d);
//    // We can control the gradient with smoothstep.
//    d = smoothstep(0.0, 0.1, d);
//    return half4(d, d, d, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // length calculates the vector distance.
//    // sine-distance functions: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbE1rZVNSVjctNkJaUGFaYUlid1BCRDJRbUJ6QXxBQ3Jtc0tucnAyY1FhT1FnYWlEM2g4bnE2cnkxbVZ5NDctUjF5ZzZnOTA1V2NrNEpBQ0l6SUQ1RndtYkdxWVFWaGVMSWhaNmhIZzZ1bFJFY18yOHBnb19VUGl4RW1yTWJxaDNVcDZ3MXdUZDhMM0lZN3NyVFVnUQ&q=https%3A%2F%2Fiquilezles.org%2Farticles%2Fdistfunctions2d%2F&v=f4s1h2YETNY
//    float d = length(uv);
//    // --> We change d to use sine, and we add the iTime uniform.
//    //d -= 0.5;
//    d = sin(d * 8.0 + iTime) / 8.0;
//    // Adding abs changes from circle to a ring.
//    d = abs(d);
//    // step will remove any gradient making a very sharp ring.
//    // d = step(0.1, d);
//    // We can control the gradient with smoothstep.
//    d = smoothstep(0.0, 0.1, d);
//    return half4(d, d, d, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // length calculates the vector distance.
//    // sine-distance functions: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbE1rZVNSVjctNkJaUGFaYUlid1BCRDJRbUJ6QXxBQ3Jtc0tucnAyY1FhT1FnYWlEM2g4bnE2cnkxbVZ5NDctUjF5ZzZnOTA1V2NrNEpBQ0l6SUQ1RndtYkdxWVFWaGVMSWhaNmhIZzZ1bFJFY18yOHBnb19VUGl4RW1yTWJxaDNVcDZ3MXdUZDhMM0lZN3NyVFVnUQ&q=https%3A%2F%2Fiquilezles.org%2Farticles%2Fdistfunctions2d%2F&v=f4s1h2YETNY
//    float d = length(uv);
//    // We change d to use sine, and we add the iTime uniform.
//    d = sin(d * 8.0 + iTime) / 8.0;
//    // Adding abs changes from circle to a ring.
//    d = abs(d);
//    // step will remove any gradient making a very sharp ring.
//    // d = step(0.1, d);
//    // We can control the gradient with smoothstep.
//    // --> Let' replace smoothstep with 1/x.
//    //d = smoothstep(0.0, 0.1, d);
//    d = 0.02 / d;
//    return half4(d, d, d, 1);

//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // length calculates the vector distance.
//    // sine-distance functions: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbE1rZVNSVjctNkJaUGFaYUlid1BCRDJRbUJ6QXxBQ3Jtc0tucnAyY1FhT1FnYWlEM2g4bnE2cnkxbVZ5NDctUjF5ZzZnOTA1V2NrNEpBQ0l6SUQ1RndtYkdxWVFWaGVMSWhaNmhIZzZ1bFJFY18yOHBnb19VUGl4RW1yTWJxaDNVcDZ3MXdUZDhMM0lZN3NyVFVnUQ&q=https%3A%2F%2Fiquilezles.org%2Farticles%2Fdistfunctions2d%2F&v=f4s1h2YETNY
//    float d = length(uv);
//    // --> We are calling a color pallete function above.
//    float3 col = palette(d);
//    // We change d to use sine, and we add the iTime uniform.
//    d = sin(d * 8.0 + iTime) / 8.0;
//    // Adding abs changes from circle to a ring.
//    d = abs(d);
//    // replace smoothstep with 1/x.
//    d = 0.02 / d;
//    // --> modify the color
//    col *= d;
//    return half4(col.r, col.g, col.b, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // length calculates the vector distance.
//    // sine-distance functions: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbE1rZVNSVjctNkJaUGFaYUlid1BCRDJRbUJ6QXxBQ3Jtc0tucnAyY1FhT1FnYWlEM2g4bnE2cnkxbVZ5NDctUjF5ZzZnOTA1V2NrNEpBQ0l6SUQ1RndtYkdxWVFWaGVMSWhaNmhIZzZ1bFJFY18yOHBnb19VUGl4RW1yTWJxaDNVcDZ3MXdUZDhMM0lZN3NyVFVnUQ&q=https%3A%2F%2Fiquilezles.org%2Farticles%2Fdistfunctions2d%2F&v=f4s1h2YETNY
//    float d = length(uv);
//    // We are calling a color pallete function above.
//    // --> Adding iTime in the param here makes colors cycle even more.
//    float3 col = palette(d + iTime);
//    // We change d to use sine, and we add the iTime uniform.
//    d = sin(d * 8.0 + iTime) / 8.0;
//    // Adding abs changes from circle to a ring.
//    d = abs(d);
//    // replace smoothstep with 1/x.
//    d = 0.02 / d;
//    // modify the color
//    col *= d;
//    return half4(col.r, col.g, col.b, 1);
    
//    // Calc uv (-1...1). Shader toy passed in iResolution. Us too.
//    // Flip uv.yy axis since Metal puts 0 at the top of the view.
//    // Account for non-square canvas (changed iResolution to 400,300 to test).
//    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
//    // --> Apply fractions.
//    //uv = fract(uv); // This only shows the bottom left (-1 .. 0) portion.
//    // --> We can fix this the same way we offset uv with iResolution originally.
//    // uv *= 2.0;
//    // uv = fract(uv);
//    // uv -= 0.5;
//    // --> for brevity
//    uv = fract(uv * 2.0) - 0.5;
//    // length calculates the vector distance.
//    // sine-distance functions: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbE1rZVNSVjctNkJaUGFaYUlid1BCRDJRbUJ6QXxBQ3Jtc0tucnAyY1FhT1FnYWlEM2g4bnE2cnkxbVZ5NDctUjF5ZzZnOTA1V2NrNEpBQ0l6SUQ1RndtYkdxWVFWaGVMSWhaNmhIZzZ1bFJFY18yOHBnb19VUGl4RW1yTWJxaDNVcDZ3MXdUZDhMM0lZN3NyVFVnUQ&q=https%3A%2F%2Fiquilezles.org%2Farticles%2Fdistfunctions2d%2F&v=f4s1h2YETNY
//    float d = length(uv);
//    // We are calling a color pallete function above.
//    // Adding iTime in the param here makes colors cycle even more.
//    float3 col = palette(d + iTime);
//    // We change d to use sine, and we add the iTime uniform.
//    d = sin(d * 8.0 + iTime) / 8.0;
//    // Adding abs changes from circle to a ring.
//    d = abs(d);
//    // replace smoothstep with 1/x.
//    d = 0.02 / d;
//    // modify the color
//    col *= d;
//    return half4(col.r, col.g, col.b, 1);
    
    
    float2 iResolution = iBoundingRect.zw;
    // Flip uv.yy axis since Metal puts 0 at the top of the view.
    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
    float2 uv0 = uv;
    float3 finalColor = float3(0.0);
    for(float i = 0.0; i < loops; i++) {
        uv = fract(uv * 1.5) - 0.5;
        float d = length(uv) * exp(-length(uv0));
        float3 col = palette(length(uv0) + i * colorTime + iTime * colorTime);
        d = sin(d * sinFreq + iTime) / sinFreq;
        d = abs(d);
        d = pow(0.01 / d, 1.2);
        finalColor += col * d;
    }
    return half4(finalColor.r, finalColor.g, finalColor.b, 1);
}
