//
//  KishimisuView.swift
//  MetalShaders
//
//  Created by Zakk Hoyt on 7/16/23.
//
//  This demo was ported over from a GLSL video: https://www.youtube.com/@kishimisu
//  See README.md for details

import SwiftUI

@available(macOS 14.0, *)
struct KishimisuView: View {
    private let startDate = Date()
    @State private var loops: Double = 4
    @State private var sinFrequency: Double = 8
    @State private var colorTime: Double = 0.4

    var body: some View {
        VStack(spacing: 8) {
            Text("kishimisu")
            TimelineView(.animation) { context in
                Rectangle()
                    .colorEffect(
                        ShaderLibrary.kishimisu(
                            .boundingRect, // Converts to iResolution
                            .float(Date().timeIntervalSince(startDate)), // Converts to iTime
                            .float(loops),
                            .float(sinFrequency),
                            .float(colorTime)
                        )
                    )
            }
            
            Slider(value: $loops, in: 1.0...5.0, step: 1.0) { Text("Loops") }
            Slider(value: $sinFrequency, in: 1.0...16.0, step: 1.0) { Text("Sin Freq") }
            Slider(value: $colorTime, in: 0.1...2, step: 0.1) { Text("Color Time") }
        }
        .padding()
    }
}

#Preview {
    KishimisuView()
}

