//
//  MetalShadersApp.swift
//  MetalShaders
//
//  Created by Zakk Hoyt on 7/16/23.
//
//  This demo was ported over from a GLSL video: https://www.youtube.com/@kishimisu
//  See README.md for details

import SwiftUI

@main
struct MetalShadersApp: App {
    var body: some Scene {
        WindowGroup {
            KishimisuView()
        }
    }
}
