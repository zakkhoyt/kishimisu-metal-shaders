
# WebGL Shaders vs Metal Shaders

WebGLSL (Open GL Shader Language) and MetalSL (Metal Shader Language) are both GPU based drawing/rendering systems. The are similar yet different in their syntax. I recently converted a shader and took note of the problems I encountered and how I solved them, and that's what this document will cover. 

For context, here is the shader before/after. 
* [Conversion Input](https://www.shadertoy.com/view/mtyGWy) (GLSL)
* [Conversion Output](https://gitlab.com/zakkhoyt/kishimisu-metal-shaders) (MetalSL)

## Requirements
* Introduced in Xcode 15 / Swift 5.9 / MacOS Sonoma

I thought I'd mention that changes to the Metal shaders do render in SwiftUI previews which is handy. 
 
# Converting GLSL to MetalSL
## References

### MetalSL
* [Metal Shading Lanuage spec](https://developer.apple.com/metal/Metal-Shading-Language-Specification.pdf) - The most important reference when translating. 
* [SwiftUI shaders](https://developer.apple.com/documentation/swiftui/shader)
* [Capturing a metal workload in Xcode](https://developer.apple.com/documentation/xcode/capturing-a-metal-workload-in-xcode)
* [Debugging Shaders](https://developer.apple.com/documentation/xcode/debugging-the-shaders-within-a-draw-command-or-compute-dispatch/)

### Learning GLSL
* [GLSL Fundamentals](https://webglfundamentals.org/webgl/lessons/webgl-shadertoy.html)
* [shadertoy tutorial](https://github.com/jingtaoh/ShaderToy-Tutorial)


### GLSL Galleries / Editor
* [shadertoy.com](https://www.shadertoy.com/)
* [shaderfrog.com](https://www.shaderfrog.com/app)
  * [glsl-parser github](https://github.com/ShaderFrog/glsl-parser)
* [glslsandbox.com](https://glslsandbox.com/)

### GLSL IDE
* [glsl.app](https://glsl.app/) - Online IDE (no browsing)
  * [github](https://github.com/zeokku/glsl.app?tab=readme-ov-file)

### Other / Similar
* [codesandbox.io](https://codesandbox.io/p/sandbox/6x1p9ownmr?file=%2Fsrc%2Findex.js) - JS / WebGL IDE
* [geexlab](https://geeks3d.com/geexlab/) - Desktop cross-platform tool for 3D programming (download required)


## Function parameters and return types
Let's begin by looking at the shader function signatures. 

```c++
// GLSL
void mainImage(
  out vec4 fragColor, 
  in vec2 fragCoord
) { ... }
```

```c++
// MSL
[[ stitchable ]]
half4 kishimisu(
    float2 position,
    half4 currentColor,
    float4 iBoundingRect,
    float iTime
) { ... }
```

### Function Parameters
* GLSL was written for [ShaderToy](https://www.shadertoy.com/) which provides two uniform values by default (`iResolution` and `iTime`). 
* MSL does not provide `iResolution` and `iTime`, but we can provide them ourselves by passing them into the shader as uniform parameters. To see how this was accomplished, see [MetalShaders/KishimisuView.swift](https://gitlab.com/zakkhoyt/kishimisu-metal-shaders/-/blob/main/MetalShaders/KishimisuView.swift).


### Return Values
* GLSL uses the `in` and `out` keywords. To return the pixel color the, values are written into the `fragColor` parameter. No use of the `return` keyword. 
* MSL instantiates and returns a `half4` instance using the `return` keyword. 

## Variable Types
Let's take a look at both of the final functions:

```c++
// GLSL
void mainImage(
  out vec4 fragColor, 
  in vec2 fragCoord
) {
    vec2 uv = (fragCoord * 2.0 - iResolution.xy) / iResolution.y;
    vec2 uv0 = uv;
    vec3 finalColor = vec3(0.0);
    
    for (float i = 0.0; i < 4.0; i++) {
        uv = fract(uv * 1.5) - 0.5;
        float d = length(uv) * exp(-length(uv0));
        vec3 col = palette(length(uv0) + i*.4 + iTime*.4);
        d = sin(d*8. + iTime)/8.;
        d = abs(d);
        d = pow(0.01 / d, 1.2);
        finalColor += col * d;
    }
    fragColor = vec4(finalColor, 1.0);
}
```

```c++
// MSL
[[ stitchable ]]
half4 kishimisu(
    float2 position,
    half4 currentColor,
    float4 iBoundingRect,
    float iTime
) {
    float2 iResolution = iBoundingRect.zw;
    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
    uv = fract(uv * 2.0) - 0.5;
    float d = length(uv);
    float3 col = palette(d + iTime);
    d = sin(d * 8.0 + iTime) / 8.0;
    d = abs(d);
    d = 0.02 / d;
    col *= d;
    return half4(col.r, col.g, col.b, 1);
}
```

The two functions return data of different (but very similar) types. Both are vectors with 4 values
* GLSL works with:
  * `vec4`, `vec3`, `vec2`
* MSL works with similar types, but of two flavors:
  * Full float values: `float4`, `float3`, `float2`
  * Half float values: `half4`, `half3`, `half2`

## Comparing Standard Libraries

As I ported this shader to Metal, I found that all of the GLSL utility functions used in kishimisu's shader do exist in the [Metal Standard Library (pdf)](https://developer.apple.com/metal/Metal-Shading-Language-Specification.pdf) as well. No adjustments were necessary. Some examples:
* `step`
* `smoothstep`
* `length`
* `abs`

However not everthing has a direct equivalent. For example:
* `texture(...)`. 
  * MSL does have `texture1d<T>(...)`, `texture2d<T>(...)`, `texture3d<T>(...)` and so on, but the syntax will differe from GLSL. 
* `mod(float,float)`
  * MSL has `fmod(float, float)`
## Bells and Whistles

After converting the final shader to Metal, I thought it would be interesting to control some of the parameters with UI controls (like you can do on shadertory). I added 3 sliders where the values are injected as additional parameters to the shader function. 

The final Metal shader:

```c++
[[ stitchable ]]
half4 kishimisu(
    float2 position,      // require param for metal shader
    half4 currentColor,  // require param for metal shader
    float4 iBoundingRect, // equivalent of shadertoy uniform (.zw)
    float iTime,          // equivalent of shadertoy uniform
    float loops,          // number of iterations in for loop
    float sinFreq,        // circle repeating frequency
    float colorTime       // how fast the colors change
) {
    float2 iResolution = iBoundingRect.zw;
    // The uv set up differs from kishimisu's video in one aspect.
    // Metal places the origin (0, 0) at the top left of the view, but
    // WebGL places the origin at the bottom left of the view.
    // I find the WebGL approach easier, so we invert uv.y by
    // multiplying by `float2(1, -1)`.
    float2 uv = ((position * 2.0 - iResolution.xy) / iResolution.y) * float2(1, -1);
    float2 uv0 = uv;
    float3 finalColor = float3(0.0);
    for(float i = 0.0; i < loops; i++) {
        uv = fract(uv * 1.5) - 0.5;
        float d = length(uv) * exp(-length(uv0));
        float3 col = palette(length(uv0) + i * colorTime + iTime * colorTime);
        d = sin(d * sinFreq + iTime) / sinFreq;
        d = abs(d);
        d = pow(0.01 / d, 1.2);
        finalColor += col * d;
    }
    return half4(finalColor.r, finalColor.g, finalColor.b, 1);
}
```

![SwiftUI_code](Documents/Images/SwiftUI_code.png)



# Assets

Here is the MetalSL shader running in SwiftUI Preview Canvas:
![SwiftUI_preview](Documents/Images/SwiftUI_preview.png)